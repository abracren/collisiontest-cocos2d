g_layer=null;
g_sprite=null;
g_sprite2=null;
g_sprite3=null;

_sprites=null;
var PlayLayer = cc.Layer.extend({
    // 1.
    space:null,// chipmunk space

    // constructor
    ctor:function () {
        this._super();
        this.init();
    },

    init:function () {
        this._super();
        //this.initPhysics();
        // 2.
        this.scheduleUpdate();
        g_layer = this;
        
        
        
        g_sprite = cc.Sprite.create(s_HelloWorld);
        g_sprite.setPosition(100,180);
        this.addChild(g_sprite);
        g_sprite.setScale(0.1);
        //var unoRect = sprite.getBoundingBox();
        
        g_sprite2 = cc.Sprite.create(s_HelloWorld);
        g_sprite2.setPosition(380,180);
        this.addChild(g_sprite2);
        g_sprite2.setScale(0.1);
    
        g_sprite3 = cc.Sprite.create(s_HelloWorld);
        g_sprite3.setPosition(240,300);
        this.addChild(g_sprite3);
        g_sprite3.setScale(0.1);

        //var dosRect = sprite.getBoundingBox();
        
         var unoRect = g_sprite.getBoundingBox();
        var dosRect =  g_sprite2.getBoundingBox();
        

        _sprites = [];
        _sprites.push(g_sprite2);
        _sprites.push(g_sprite3);
        cc.log(_sprites);


        
    
      

        this.schedule(this.reboot,2.1);

        
        
        
    },
    reboot:function(){
    
            
        var actionTo = cc.MoveTo.create(1, cc.p(100, 180));
        var actionTo2 = cc.MoveTo.create(1, cc.p(380, 180));
        var actionTo3 = cc.MoveTo.create(.5, cc.p(240, 180));
        var actionTo4 = cc.MoveTo.create(.5, cc.p(240, 300));




        g_sprite2.runAction(cc.Sequence.create(actionTo,cc.DelayTime.create(0.1),actionTo2));
        g_sprite3.runAction(cc.Sequence.create(actionTo3,cc.DelayTime.create(0.1),actionTo4));


    
    },

    // 3..
    
    update:function (dt) {
    var i;
           // cc.log(_sprites);

    for(i in _sprites){
    //var enemyBulletsKill = _sprites[i];
    //var enemyBulletRect =  _sprites[i].getBoundingBox();


    if (cc.rectIntersectsRect(g_sprite.getBoundingBox(),_sprites[i].getBoundingBox())){
         _sprites[i].setScale(0.2);
        }else{
         _sprites[i].setScale(0.1);
        }
    

    }
         
        // 7.
        //this.space.step(dt);
    }
    });

PlayLayer.scene = function () {
    var scene = cc.Scene.create();
    var layer = new PlayLayer();
    scene.addChild(layer);
    return scene;
};