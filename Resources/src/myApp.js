require("src/backLayer.js");
require("src/utils.js");

var MyLayer = cc.Layer.extend({
     // 3.
    ctor:function () {
        this._super();
        this.init();
    },

    // 4.
    init:function () {
        this._super();
        var centerPos = cc.p(winSize.width / 2, winSize.height / 2);
        /*
        var spriteBG = cc.Sprite.create("res/HelloWorld.png");
        spriteBG.setPosition(centerPos);
        this.addChild(spriteBG);
        */

        cc.MenuItemFont.setFontSize(60);
        var menuItemPlay = cc.MenuItemFont.create("Play", this.onPlay, this);
        var menu = cc.Menu.create(menuItemPlay);
        menu.setPosition(centerPos);
        this.addChild(menu);
    },

    // on play button clicked
    onPlay:function (sender) {
        // 5.
    cc.Director.getInstance().replaceScene(PlayLayer.scene());
    }
});
var MyScene = cc.Scene.extend({
    ctor:function() {
        this._super();
        cc.associateWithNative( this, cc.Scene );
    },

    onEnter:function () {
        this._super();
        var layer = new MyLayer();
        this.addChild(layer);
        layer.init();
    }
});
try {
    // 7.
    director = cc.Director.getInstance();
    winSize = director.getWinSize();
    // run first scene
    //director.runWithScene(MyScene.scene());
} catch(e) {log(e);}
